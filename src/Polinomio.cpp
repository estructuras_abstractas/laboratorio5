/**
 * @author Jorge Munoz Taylor
 * @date 31-01-2020
 *
 * @file Polinomio.cpp
 * @brief Se definen los métodos de la clase Polinomio.
 */


#include <math.h> /**< En esta biblioteca se incluye el método fabs. */
#include "../include/Polinomio.h"

using namespace std;

template <typename Data>
Polinomio<Data>::Polinomio(Data X, Data Y, Data Z){
    x = X;
    y = Y;
    z = Z;
    mul_valida = false; ///< Dice si se hizo una multiplicación.
    division_valida = 0; ///< Indica si se hizo una división.
    return;
}


template <typename Data>
void Polinomio<Data>::operator+(const Polinomio<Data> &rhs)
{
    this->add(x, rhs.x);
    this->add(y, rhs.y);
    this->add(z, rhs.z);
    return;
}

template <typename Data>
void Polinomio<Data>::operator-(const Polinomio &rhs)
{
    this->sub(x, rhs.x);
    this->sub(y, rhs.y);
    this->sub(z, rhs.z);
    return;
}

template <typename Data>
void Polinomio<Data>::operator*(const Polinomio &rhs)
{
    mul_valida = true;

    a = x * rhs.x;
    b = x * rhs.y + y * rhs.x;
    c = x * rhs.z + y * rhs.y + z * rhs.x;
    d = y * rhs.z + z * rhs.y;
    e = z * rhs.z;

    return;
}

template <typename Data>
void Polinomio<Data>::operator/(const Polinomio &rhs)
{

    Data dividendo[3];
    Data divisor[3];

    dividendo[0] = z;
    dividendo[1] = y;
    dividendo[2] = x;
    divisor[0]   = rhs.z;
    divisor[1]   = rhs.y;
    divisor[2]   = rhs.x;

    if(x == 0 && rhs.x != 0) division_valida = 2; ///< Grado del dividendo debe ser mayor.
    
    else if( x == 0 && y == 0 && z == 0) division_valida = 3; ///< Cociente nulo.
    
    else if(x == 0 && rhs.x == 0 && y == 0 && rhs.y != 0) division_valida = 2; ////< Grado del dividendo debe ser mayor.
    
    else if( rhs.x == 0 && rhs.y == 0 && rhs.z == 0) division_valida = 4; ///< Dvision por cero.
    
    else
    {
        division_valida = 1;

        for(int i = 0; i < 3; i++){

            if(dividendo[2-i] != 0){
                for(int j = 0; j < 3; j++){
                
                    if(dividendo[2-j] != 0){
                        cociente = dividendo[2-i];
                        this->div(cociente, divisor[2-j]);
                        j = 3;
                    }
                
                }//Fin de for
                i = 3;
            }//Fin de if
        }//Fin de for

        residuo[1] = dividendo[1] - cociente*divisor[1];
        residuo[0] = dividendo[0] - cociente*divisor[0];

    } //Fin de else
    
    return;
}


template <typename Data>
Polinomio<Data>& Polinomio<Data>::operator=(const Polinomio<Data>& rhs)
{
    string temp;
    string simbolo4, simbolo3, simbolo2, simbolo1, simbolo0;
    string tempx, tempy, tempz;
    string tempa, tempb, tempc, tempd, tempe;
    string x4, x3, x2, x11;

    tempx = to_string (fabs(x));
    tempy = to_string (fabs(y));
    tempz = to_string (fabs(z)); 

    tempa = to_string (fabs(a));
    tempb = to_string (fabs(b));
    tempc = to_string (fabs(c));
    tempd = to_string (fabs(d));
    tempe = to_string (fabs(e));

    temp = "Resultado: ";
    x4   = "x^4";
    x3   = "x^3";
    x2   = "x^2";
    x11  = "x";

    if(mul_valida)
    {
        if(a >= 0) simbolo0 = " + ";
        else simbolo0 = " - ";

        if(b >= 0) simbolo4 = " + ";
        else simbolo4 = " - ";

        if(c >= 0) simbolo3 = " + ";
        else simbolo3 = " - ";

        if(d >= 0) simbolo2 = " + ";
        else simbolo2 = " - ";

        if(e >= 0) simbolo1 = " + ";
        else simbolo1 = " - ";

        this->resultado = temp + simbolo0 + tempa + x4 + simbolo4 + tempb + x3 + simbolo3 + tempc + x2 + simbolo2 + tempd + x11 + simbolo1 + tempe;
    }

    else if( division_valida == 1)
    {
        string coci = "Cociente: ";
        string rest = "   | Residuo: ";
        string x1 = "x ";
        string simbolo;
        string resi1 = to_string(residuo[1]);
        string resi0 = to_string(fabs(residuo[0]));
        string coci_string = to_string(cociente);

        if(residuo[0] >= 0) simbolo = " + ";
        else simbolo = " - ";

        this->resultado = coci + coci_string + rest + resi1 + x1 + simbolo + resi0; 
    }

    else if( division_valida == 3)
    {
        tempa = "El polinomio-dividendo no puede ser nulo :V";
        this->resultado = tempa;
    }

    else if( division_valida == 2)
    {
        tempa = "El grado del polinomio-dividendo debe ser igual o mayor al grado del divisor :V";
        this->resultado = tempa;
    }

    else if( division_valida == 4)
    {
        tempa = "El polinomio-divisor no puede ser nulo :V";
        this->resultado = tempa;
    }

    else
    {
        if(y >= 0) simbolo2 = " + ";
        else simbolo2 = " - ";

        if(z >= 0) simbolo1 = " + ";
        else simbolo1 = " - ";

        if(x >= 0) simbolo0 = "";
        else simbolo0 = " - ";

        this->resultado = temp + simbolo0 + tempx + x2 + simbolo2 + tempy + x11 + simbolo1 + tempz;
    }

    cout << endl << "Resultado: " << this->resultado;
}