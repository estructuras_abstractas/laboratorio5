/**
 * @author Jorge Munoz Taylor
 * @date 31-01-2020
 *
 * @file Fraccion.cpp
 * @brief En este archivo se definen los métodos de la clase Fraccion.
 */


#include "../include/Fraccion.h"

using namespace std;

template <typename Data>
Fraccion<Data>::Fraccion(Data X, Data Y)
{
    x = X;
    y = Y;

    return;
}


template <typename Data>
void Fraccion<Data>::operator+(const Fraccion<Data> &rhs)
{    
    fraccion1 = x/y;
    fraccion2 = rhs.x / rhs.y;

    this->add ( fraccion1, fraccion2 );


    return;
}

template <typename Data>
void Fraccion<Data>::operator-(const Fraccion &rhs)
{    
    fraccion1 = x/y;
    fraccion2 = rhs.x / rhs.y;

    this->sub ( fraccion1, fraccion2 );

    return;
}

template <typename Data>
void Fraccion<Data>::operator*(const Fraccion &rhs)
{
    fraccion1 = x/y;
    fraccion2 = rhs.x / rhs.y;

    this->mul ( fraccion1, fraccion2 );
    
    return;
}

template <typename Data>
void Fraccion<Data>::operator/(const Fraccion &rhs)
{    
    fraccion1 = x/y;
    fraccion2 = rhs.x / rhs.y;

    this->div ( fraccion1, fraccion2 );

    return;
}


template <typename Data>
Fraccion<Data>& Fraccion<Data>::operator=(const Fraccion<Data>& rhs)
{
    this->print( fraccion1 );
}