/**
 * @author Jorge Munoz Taylor
 * @date 31-01-2020
 *
 * @file main.cpp
 * @brief Contiene los algoritmos y estructuras de control que describen 
 * el funcionamiento del programa.
 */


#include <iostream>
#include <string>
#include <stdlib.h> /**< En esta biblioteca se incluye el método atof. */

#include "../include/Calculadora.h"
#include "../include/Fraccion.h"
#include "../include/Polinomio.h"

#define EXITO 0
#define ERROR 1

#define ALGUN_PARAMETRO_MALO 0
#define DIVISION_POR_0 1
#define SI_ES_FRAC_POLI 2

using namespace std;


/**
 * @brief Este método verifica que se hayan introducido números y signos de operación válidos,
 * en este caso regresa 2, si algún elemento no es correcto devuelve 0, si se detecta que hay
 * un 0 en algún divisor regresa 1.
 */
template <typename Data>
unsigned short int Verifica_introdujeron_numeros ( Data A, char** C)
{
    //Regresa 0 si algun elemento es incorrecto
    //Regresa 1 si hay division entre 0
    //Regresa 2 si todo salio bien

    ///< Se está operando con fracciones.
    if(A == 8 && C[2][0] == '/' && C[6][0] == '/' ){ ///< Verifica si se ingresaron 7 elementos (uno de ellos es el nombre del programa).
        
        if( C[1] == to_string(0) || atof(C[1]) ){} ///< Se compara el parámetro para saber si se trata de un 0 o un número real.
        else return ALGUN_PARAMETRO_MALO;

        if( atof(C[3]) || C[3] == to_string(0)){
            if(C[3] == to_string(0)) return DIVISION_POR_0;
        }
        else return ALGUN_PARAMETRO_MALO;

        if( C[4][0] == '+' || C[4][0] == '-' || C[4][0] == '*' || C[4][0] == '/'){} ///< Verifica que el argumento sea alguna operación válida
        else return ALGUN_PARAMETRO_MALO;

        if( C[5] == to_string(0) || atof(C[5]) ){}
        else return ALGUN_PARAMETRO_MALO;

        if( atof(C[7]) || C[7] == to_string(0) ){ ///< Aquí se comprueba si los parámetros divisores son cero.
            if(C[7] == to_string(0)) return DIVISION_POR_0; ///< @return Regresa 1 porque al ser 0 se producirá una división entre 0.
        }
        else return ALGUN_PARAMETRO_MALO;

        return SI_ES_FRAC_POLI; ///< Todo salió bien.       
    } //Fin de if


    ///< Comprueba si se está operando con polinomios. 
    if(A == 8 && ( C[4][0] == '+' || C[4][0] == '-' || C[4][0] == '*' || C[4][0] == '/' )){

        if( C[1] == to_string(0) || atof(C[1]) ){}
        else return ALGUN_PARAMETRO_MALO;

        if( C[2] == to_string(0) || atof(C[2]) ){}
        else return ALGUN_PARAMETRO_MALO;

        if( C[3] == to_string(0) || atof(C[3]) ){}
        else return ALGUN_PARAMETRO_MALO;


        if( C[5] == to_string(0) || atof(C[5]) ){}
        else return ALGUN_PARAMETRO_MALO;

        if( C[6] == to_string(0) || atof(C[6]) ){}
        else return ALGUN_PARAMETRO_MALO;

        if( C[7] == to_string(0) || atof(C[7]) ){}
        else return ALGUN_PARAMETRO_MALO;

        return SI_ES_FRAC_POLI;            
        
    } //Fin de if


    if(A == 4){///< Comprueba si se está realizando una peración con números reales o enteros.
        
        if( C[1] == to_string(0) || atof(C[1]) ){}
        else return 0;

        if( C[2][0] == '+' || C[2][0] == '-' || C[2][0] == '*' || C[2][0] == '/'){}
        else return 0;

        if( C[3] == to_string(0) || atof(C[3]) ){}
        else return 0;

        return 2;     
    }
}


/** 
 * @brief En la función main se encuentra el algoritmo de control del programa, es decir
 * contiene las estructuras necesarias para crear y administrar las clases, toma de parámetros
 * y algoritmos para hacer funcionar la calculadora.
 */
int main ( int argc, char** argv )
{ ///< Los argumentos se pasan por la terminal, ver README.

    ///< Declaracion de objetos y variables.
    Calculadora<float> calcu_float;
    Calculadora<int> calcu_int;
    Fraccion<float>* frac;
    Fraccion<float>* frac2;
    Polinomio<float>* pol1;
    Polinomio<float>* pol2;
    
    float x1 , x12 , x13;
    float y1 , y12 , y13;
    int x2, y2, w2, z2;

    int ARG = argc;
    char** ARGV = argv;

    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

    ///< Verifica si se introdujo algun parámetro
    if(ARGV[1] != NULL){

        if( Verifica_introdujeron_numeros<float>(ARG, ARGV) == ALGUN_PARAMETRO_MALO) {
            cout << endl << "Revise los parametros que introdujo, recuerde que la multiplicacion" << endl;
            cout << "debe escribirse asi '*', ademas este programa resuelve unicamente polinomios de";
            cout << endl << "grado 3 por lo que debe proveer 2 polinomios de 3 elementos y la operacion" << endl;
            cout << "por lo tanto tiene que escribir 7 argumentos, ejemplo:" << endl << endl;
            cout << "     ./OUT 1 2 3 + 4 5 6" << endl;
            cout << endl << "Lo que equivale a:" << endl << endl;
            cout << "     (1x^2 + 2x + 3) + (4x^2 + 5x + 6)" << endl;
            cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
            return ERROR;
        }
        else if( Verifica_introdujeron_numeros<float>(ARG, ARGV) == DIVISION_POR_0 ){
            cout << endl << "Hay un cero en el denominador =/" << endl;
            cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
            return ERROR;
        }
        else if ( Verifica_introdujeron_numeros<float>(ARG, ARGV) == SI_ES_FRAC_POLI ){}
        else {
            cout << endl << "ERROR en el codigo" << endl;
            cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
            return ERROR;
        }


        ///< Verifica si se trata de un polinomio
        if(  atof(ARGV[1]) && atof( ARGV[2] ) && atof( ARGV[3] ) && atof( ARGV[5] ) && atof( ARGV[6] ) && atof( ARGV[7] ))
        {
            x1  = atof( ARGV[1] );
            x12 = atof( ARGV[2] );
            x13 = atof( ARGV[3] );
            y1  = atof( ARGV[5] );
            y12 = atof( ARGV[6] );
            y13 = atof( ARGV[7] );
            
            pol1 = new Polinomio<float>(x1, x12, x13);
            pol2 = new Polinomio<float>(y1, y12, y13);
            
            if(ARGV[4][0] == '+'){
                (*pol1) + (*pol2);
                (*pol1) = (*pol2);
            }
            else if(ARGV[4][0] == '-'){
                (*pol1) - (*pol2);
                (*pol1) = (*pol2);
            }
            else if(ARGV[4][0] == '*'){
                (*pol1) * (*pol2);
                (*pol1) = (*pol2);
            }
            else if(ARGV[4][0] == '/'){
                (*pol1) / (*pol2);
                (*pol1) = (*pol2);
            }
            else cout << endl << "Esa operacion no existe :(";

            delete pol1;
            delete pol2;

        } //Fin de if


        //< Se usarán fracciones.
        else
        {
            switch(ARG){

                case 4: ///< Para 3 argumentos, aqui se calculan los enteros y reales

                    x1 = atof(ARGV[1]);
                    y1 = atof(ARGV[3]);

                    if     ( ARGV[2][0] == '+' ) calcu_float.add(x1, y1);
                    else if( ARGV[2][0] == '-' ) calcu_float.sub(x1, y1);
                    else if( ARGV[2][0] == '*' ) calcu_float.mul(x1, y1);
                    else if( ARGV[2][0] == '/' ) calcu_float.div(x1, y1);
                    else
                    {
                        cout << endl << "Esa operacion no existe =(" ;
                        cout << endl;
                        break;
                    }

                    calcu_float.print(x1);
                    break;
                                                    


                case 8: ///< Para 7 argumentos, aqui se calculan las fracciones

                    x1  = atof(ARGV[1]);
                    x12 = atof(ARGV[3]);
                    x13 = atof(ARGV[5]);
                    y1  = atof(ARGV[7]);

                    frac  = new Fraccion <float>(x1, x12);
                    frac2 = new Fraccion <float>(x13, y1);

                    if( ARGV[2][0] == '/' && ARGV[6][0] == '/' )
                    {
                        if( ARGV[4][0] == '+' ) 
                        {
                            (*frac) + (*frac2) ; 
                            (*frac) = (*frac2);  
                        
                        }

                        else if( ARGV[4][0] == '-' ) 
                        {
                            (*frac) - (*frac2) ; 
                            (*frac) = (*frac2);
                        }

                        else if( ARGV[4][0] == '*' ) 
                        {
                            (*frac) * (*frac2) ; 
                            (*frac) = (*frac2); 
                        }

                        else if( ARGV[4][0] == '/' ) 
                        {
                            (*frac) / (*frac2) ; 
                            (*frac) = (*frac2);
                        }

                        else
                        {
                            cout << endl << "Esa operacion no existe =(" ;
                            cout << endl;
                            delete frac;
                            delete frac2;
                            break;
                        }
                    }

                    else
                    {
                        cout << endl;
                        cout << "Induzca el simbolo / para las fracciones =)";
                        cout << endl;
                        delete frac;
                        delete frac2;
                        break;
                    }

                    delete frac;
                    delete frac2;
                    break;
                    

                    
                default:
                    cout << endl;
                    cout << "El programa funciona desde la linea de comandos";
                    cout << ", si quiere hacer operaciones con numeros enteros o reales";
                    cout << " escriba" << endl << "       ./OUT A + B " << endl;
                    cout << "Donde + puede ser tambien:" << endl;
                    cout << "       -" << endl << "       /" << endl << "       /*" << endl;
                    cout << endl << "Note que los espacios deben ser tal como se muestran de lo contrario";
                    cout << " el programa no funcionara como debe, tambien tenga en cuenta que el operador";
                    cout << " * solo se puede escribir de la siguiente forma '*'.";
                    cout << endl << endl;
                    cout << "Si quiere hacer operaciones en fracciones escriba:" << endl;
                    cout << "       ./OUT A / B + C / D" << endl;
                    cout << "Respetando los espacios y la operacion puede ser otra (resta, multiplicacion o division).";
                    break;

            } //Fin de switch
            
        }//Fin del else

    }//Fin de if
    else{
        cout << endl;
        cout << "El programa funciona desde la linea de comandos";
        cout << ", si quiere hacer operaciones con numeros enteros o reales";
        cout << " escriba" << endl << "       ./OUT A + B " << endl;
        cout << "Donde + puede ser tambien:" << endl;
        cout << "  -" << endl << "  /" << endl << "  /*" << endl;
        cout << endl << "Note que los espacios deben ser tal como se muestran de lo contrario";
        cout << " el programa no funcionara como debe, tambien tenga en cuenta que el operador";
        cout << " * solo se puede escribir de la siguiente forma '*'.";
    }

    cout << endl << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
    
    return EXITO;
} //Fin de init