/**
 * @author Jorge Munoz Taylor
 * @date 31-01-2020
 *
 * @file Calculadora.cpp
 * @brief En este archivo se definen los métodos de la clase Calculadora.
 */


#include <iostream>
#include "../include/Calculadora.h"

using namespace std;

template <typename Data>
void Calculadora<Data>::add(Data &d1, const Data &d2)
{
    d1 = d1 + d2;
    return;
}

template <typename Data>
void Calculadora<Data>::sub(Data &d1, const Data &d2)
{
    d1 = d1 - d2;
    return;
}

template <typename Data>
void Calculadora<Data>::mul(Data &d1, const Data &d2)
{
    d1 = d1 * d2;
    return;
}

template <typename Data>
void Calculadora<Data>::div(Data &d1, const Data &d2)
{
    d1 = d1/d2;
    return;
}

template <typename Data>
void Calculadora<Data>::print(Data &d)
{
    cout << endl << "Resultado: " << d;
    return;
}