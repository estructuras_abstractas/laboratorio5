ALL: packages comp doxygen test

packages:
	@echo "**************************************************************************"
	@echo "** Se instalarán los paquetes necesarios para que funciones el programa **"
	@echo "**************************************************************************"
	@sleep 1s

	sudo apt install g++ -y
	sudo apt-get install build-essential -y
	sudo apt install doxygen -y
	sudo apt install doxygen-gui -y
	sudo apt-get install graphviz -y
	sudo apt install texlive-latex-extra -y
	sudo apt install texlive-lang-spanish -y
	sudo apt-get install libsfml-dev -y
	sudo apt autoremove -y


comp:
	@echo "**************************************************************************"
	@echo "***********           Se generará el ejecutable            ***************"
	@echo "**************************************************************************"
	@sleep 1s

	mkdir -p bin build docs

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Calculadora.o -c ./src/Calculadora.cpp 
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Fraccion.o -c ./src/Fraccion.cpp 
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/Polinomio.o -c ./src/Polinomio.cpp 

	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./build/main.o -c ./src/main.cpp 
	g++ -D_GLIBCXX_USE_CXX11_ABI=0 -o ./bin/lab5 ./build/main.o ./build/Calculadora.o ./build/Polinomio.o ./build/Fraccion.o

doxygen:
	@echo "**************************************************************************"
	@echo "************      Generando el archivo refman.pdf          ***************"
	@echo "**************************************************************************"
	@sleep 1s

	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex

test:
	@echo "**************************************************************************"
	@echo "***********         Pruebas para Fracción                    *************"
	@echo "**************************************************************************"
	@sleep 2s
	@echo
	@echo Suma
	@./bin/lab5 1 / 2 + 2 / 3
	@echo

	@echo Resta
	@./bin/lab5 1 / 2 - 2 / 3
	@echo

	@echo Multiplicación
	@./bin/lab5 1 / 2 '*' 2 / 3
	@echo

	@echo División
	@./bin/lab5 1 / 2 / 2 / 3
	@echo

	@echo "**************************************************************************"
	@echo "************         Pruebas para Polinomio                  *************"
	@echo "**************************************************************************"
	@sleep 2s

	@echo
	@echo Suma
	@./bin/lab5 1.2 2.12 3.121 + 4.656 5.6565 6.7675
	@echo

	@echo Resta
	@./bin/lab5 1.2 2.12 3.121 - 4.656 5.6565 6.7675
	@echo

	@echo Multiplicación
	@./bin/lab5 1.2 2.12 3.121 '*' 4.656 5.6565 6.7675
	@echo

	@echo División
	@./bin/lab5 1.2 2.12 3.121 / 4.656 5.6565 6.7675
	@echo
