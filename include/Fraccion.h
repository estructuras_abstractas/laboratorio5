#pragma once

/**
 * @author Jorge Munoz Taylor
 * @date 31-01-2020
 *
 * @file Fraccion.h
 * @brief En este archivo se crea la plantilla de la clase Fraccion.
 */

#include <iostream>
#include <string>
#include "Calculadora.h"


/**
 * @class Fraccion
 * @brief La clase Fracción hereda de la clase Calculadora, se definen elementos privados y se sobrecargan varios operadores.
 */

template <typename Data>
class Fraccion : public Calculadora <Data>
{

    public:

        /**
         * @brief Constructor, en el se almacenan los datos pasados por el usuario.
         * @param X Numerador de la fracción.
         * @param Y Denominador de la fracción.
         */
        Fraccion (Data X, Data Y);

            
        /**
         * @brief Sobrecarga el operador +.
         * @param rhs Se trata de la segunda fracción sobre la que se operará.
         */
        void operator+(const Fraccion<Data> &rhs);

        /**
         * @brief Sobrecarga el operador -.
         * @param rhs Se trata de la segunda fracción sobre la que se operará.
         */
        void operator-(const Fraccion &rhs);

        /**
         * @brief Sobrecarga el operador *.
         * @param rhs Se trata de la segunda fracción sobre la que se operará.
         */
        void operator*(const Fraccion &rhs);

        /**
         * @brief Sobrecarga el operador /.
         * @param rhs Se trata de la segunda fracción sobre la que se operará.
         */
        void operator/(const Fraccion &rhs);
        
        /**
         * @brief Sobrecarga el operador =.
         * @param rhs Se trata de la segunda fracción sobre la que se operará.
         * @return 
         */
        Fraccion& operator=(const Fraccion& rhs);
        
    
    private:

        Data fraccion1; //< Valor de la primera fracción.
        Data fraccion2; //< Valor de la segunda fracción.

        Data x; //<Numerador pasado por el usuario.
        Data y; //<Denominador pasado por el usuario.

        std::string resultado;
};

template class Fraccion<float>;
template class Fraccion<int>;