#pragma once

/**
 * @author Jorge Munoz Taylor
 * @date 31-01-2020
 *
 * @file Calculadora.h
 * @brief En este archivo se crea la plantilla de la clase Calculadora.
 */

/**
 * @class Calculadora
 * @brief La clase Calculadora funciona como clase base para las clases
 * Polinomio y Fraccion, además aporta la el funcionamiento básicos para
 * las operaciones con números enteros y reales.
 */

template <typename Data>
class Calculadora
{
    public:

        /**
         * @brief Operación de suma.
         * @param d1 Dirección donde se almacenará el resultado.
         * @param d2 Nuevo dato al que se le aplicará la operación.
         */
        virtual void add (Data &d1, const Data &d2);
        
        /**
         * @brief Operación de resta.
         * @param d1 Dirección donde se almacenará el resultado.
         * @param d2 Nuevo dato al que se le aplicará la operación.
         */
        virtual void sub (Data &d1, const Data &d2);
        
        /**
         * @brief Operación de multiplicación.
         * @param d1 Dirección donde se almacenará el resultado.
         * @param d2 Nuevo dato al que se le aplicará la operación.
         */        
        virtual void mul (Data &d1, const Data &d2);

        /**
         * @brief Operación de división.
         * @param d1 Dirección donde se almacenará el resultado.
         * @param d2 Nuevo dato al que se le aplicará la operación.
         */
        virtual void div (Data &d1, const Data &d2);
        
        /**
         * @brief Imprime el resultado en la terminal.
         * @param d1 El dato que se mostrará en pantalla.
         */        
        virtual void print (Data &d);
};

template class Calculadora<float>;
template class Calculadora<int>;