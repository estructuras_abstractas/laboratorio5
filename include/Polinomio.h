#pragma once

/**
 * @author Jorge Munoz Taylor
 * @date 31-01-2020
 *
 * @file Polinomio.h
 * @brief Aquí se define la estructura de la clase Polinomio.
 */

#include <iostream>
#include <string>
#include "Calculadora.h"


/**
 * @class Polinomio
 * @brief Hereda de la clase Calculadora, además incluye elementos privados encargados de almacenar
 * los datos correspondientes al polinomio ingresado (A B C), el cociente, el residuo y las banderas
 * mul_valida y division_valida que indican si se está multiplicando o dividiendo.
 */

template <typename Data>
class Polinomio : public Calculadora <Data>{

    public:

        /**
         * @brief Constructor, guarda los datos introducidos por el usuario e inicializa variables. 
         * @param X Primer elemento del polinomio.
         * @param Y Segundo elemento del polinomio.
         * @param Z Tercer elemento del polinomio.
         */
        Polinomio (Data X, Data Y, Data Z);
    
        /**
         * @brief Sobrecarga el operador +.
         * @param rhs Polinomio que operará sobre el otro polinomio.
         */
        void operator+ (const Polinomio<Data> &rhs);
        
        /**
         * @brief Sobrecarga el operador -.
         * @param rhs Polinomio que operará sobre el otro polinomio.
         */        
        void operator- (const Polinomio &rhs);
        
        /**
         * @brief Sobrecarga el operador *.
         * @param rhs Polinomio que operará sobre el otro polinomio.
         */        
        void operator* (const Polinomio &rhs);

        /**
         * @brief Sobrecarga el operador /.
         * @param rhs Polinomio que operará sobre el otro polinomio.
         */
        void operator/ (const Polinomio &rhs);

        /**
         * @brief Sobrecarga el operador =.
         * @param rhs Polinomio que operará sobre el otro polinomio.
         * @return Regresa el polinomio resultado de la operación.
         */
        Polinomio& operator= (const Polinomio& rhs);

    
    private:
        Data x;
        Data y;
        Data z;

        Data a, b, c, d, e;

        Data cociente;
        Data residuo[2];

        bool mul_valida;//<Indica si se trata de una multiplicación.
        unsigned short int division_valida;//< Tipo de división.
        std::string resultado;
};

template class Polinomio<float>;
template class Polinomio<int>;