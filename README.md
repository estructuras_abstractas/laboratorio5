# Proyecto 5: Programación genérica en C++


## Integrante
```
Jorge Muñoz Taylor
```

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio5
```

Por último ejecute el make:
```
>>make
```

## Ejecutar el programa
Simplemente:
```
./bin/labo5
```
Para fracciones simples:
```
>> ./bin/lab5 # / #
```

Para operanciones con fracciones:
```
>> ./bin/lab5 # / # op # / #
```
Para polinomios:
```
>> ./bin/lab5 # # # op # # #
```

Donde # es un número real o entero y op es cualquiera de las 4 operaciones básicas.

## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio4
```

Para compilar el doxygen:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd DOCS/latex
>>make
```
Esto generará el archivo PDF con la documentación generada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de DOCS/latex
```

## Dependencias

Debe tener instalado MAKE en la máquina, el makefile verificará e instalará todo lo siguiente:
```
>>sudo apt-get install build-essential
```
g++:
```
sudo apt install g++
```

También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```